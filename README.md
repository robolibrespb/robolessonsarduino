# Hey ho, let's go! 

В этом репозитории хранятся материалы занятий по платформе Arduino от коллектива Либертарной Робототехники, а также некоторых сторонних библиотек, используемых нами в рамках занятий.


Доступные на данный момент материалы:  
[IR Remote Demo](https://bitbucket.org/robolibre161/robolessonsarduino/src/master/IRDevice_demo/)  


# Информация о лицензии на материалы   
Весь программный код, даже в случаях если это не указано явно внутри файлов с исходным кодом, имеет лицензию General Public License v.3  
Все медиафайлы (текст, фото- и видеоматериалы, электрические схемы итп.) имеют лицензию Creative Commons BY-SA 3.0  

Текст свободной лицензии GPLv3 доступен [здесь](https://www.gnu.org/licenses/quick-guide-gplv3.ru.html)  
О свободной лицензии CC BY-SA 3.0 можно прочесть [здесь](https://creativecommons.org/licenses/by-sa/3.0/deed.ru)  